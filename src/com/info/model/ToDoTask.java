package com.info.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ToDoTask")
public class ToDoTask {
	 @Id
	private long taskID;
	private String taskDescription;
	private boolean pending;
	
	public ToDoTask() {
		taskID=0;
	}
	
	public long getTaskID() {
		return taskID;
	}
	public void setTaskID(long taskID) {
		this.taskID = taskID;
	}
	public String getTaskDescription() {
		return taskDescription;
	}
	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (pending ? 1231 : 1237);
		result = prime * result + ((taskDescription == null) ? 0 : taskDescription.hashCode());
		result = prime * result + (int) (taskID ^ (taskID >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ToDoTask other = (ToDoTask) obj;
		if (pending != other.pending)
			return false;
		if (taskDescription == null) {
			if (other.taskDescription != null)
				return false;
		} else if (!taskDescription.equals(other.taskDescription))
			return false;
		if (taskID != other.taskID)
			return false;
		return true;
	}
	
	
	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public ToDoTask(long taskID,String taskDesc,boolean pending) {
		this.taskID=taskID;
		this.taskDescription=taskDesc;
		this.pending=pending;
		
	}
	
	
	
	
}
