package com.info.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.info.model.ToDoTask;

@Repository
@Qualifier("ToDoDaoImpl")
public class ToDoDaoImpl {
    @Autowired
    MongoTemplate mongoTemplate;
 
    final String COLLECTION = "ToDoTask";

    public void create(ToDoTask task) {
        mongoTemplate.insert(task);
    }
 
    public void update(ToDoTask task) {
        mongoTemplate.save(task);
    }
 
    public void delete(ToDoTask task) {
        mongoTemplate.remove(task);
    }
 
    public void deleteAll() {
        mongoTemplate.remove(new Query(), COLLECTION);
    }
 
    public ToDoTask find(ToDoTask task) {
        Query query = new Query(Criteria.where("_id").is(task.getTaskID()));
        return mongoTemplate.findOne(query, ToDoTask.class, COLLECTION);
    }
 
    public List < ToDoTask > findAll() {
        return (List < ToDoTask > ) mongoTemplate.findAll(ToDoTask.class);
    }



}
