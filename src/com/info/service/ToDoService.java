package com.info.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.info.dao.ToDoDaoImpl;
import com.info.model.ToDoTask;

@Service("todoService")
public class ToDoService {

	@Autowired
	ToDoDaoImpl todoDaoImpl;
	
	
	 private static final AtomicLong counter = new AtomicLong();
	 
	 private static List<ToDoTask> tasks;
	 
	 static {
		 tasks=populateDummyTasks();
	 }
	 
	 private static List<ToDoTask> populateDummyTasks(){
		 List<ToDoTask> tasks = new ArrayList();
		 tasks.add(new ToDoTask(counter.incrementAndGet(),"Sam",true));
		 tasks.add(new ToDoTask(counter.incrementAndGet(),"Ram",true));
		 tasks.add(new ToDoTask(counter.incrementAndGet(),"Shyaam",true));
	        return tasks;
	    }
	 
	public  List<ToDoTask> getAllTasks() {
//		 return tasks;
		return todoDaoImpl.findAll();
	}
	
	public ToDoTask findById(long id) {
		/*for(ToDoTask task : tasks){
            if(task.getTaskID()==(id)){
                return task;
            }
        }
        return null;*/
		for( ToDoTask task : todoDaoImpl.findAll() ){
            if(task.getTaskID()==(id)){
                return task;
            }
        }
        return null;
		
	}
	
	public boolean isTaskExist(ToDoTask task) {
//		  return findById(task.getTaskID())!=null;
		return todoDaoImpl.find(task)!=null;
	}
	
	public void saveTask(ToDoTask task) {
//		task.setTaskID(counter.incrementAndGet());
//		tasks.add(task);
		todoDaoImpl.create(task);
	}
	
	public void updateTask(ToDoTask task)
	{
//		 int index = tasks.indexOf(task);
//		 tasks.set(index, task);
		todoDaoImpl.update(task);
	}	
	
	public void deleteTaskById(long id) {
		  /*for (Iterator<ToDoTask> iterator = tasks.iterator(); iterator.hasNext(); ) {
			  ToDoTask task = iterator.next();
	            if (task.getTaskID() == id) {
	                iterator.remove();
	            }
	        }*/
		for( ToDoTask task : todoDaoImpl.findAll() ){
            if(task.getTaskID()==(id)){
            		todoDaoImpl.delete(task);
            		break;
            }
        }
		
	}
	
	public void deleteAllTasks() {
//		tasks.clear();
		todoDaoImpl.deleteAll();
	}
	
}
