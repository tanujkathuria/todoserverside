package com.info.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.info.model.Message;
import com.info.model.ToDoTask;
import com.info.service.ToDoService;

@CrossOrigin("*")
@RestController
public class HelloWorldRestController {

	
	 @RequestMapping("/")
	public String welcome() {
		return "welcome to TODO Task program";
	}
	
	 
	 @Autowired
	    ToDoService todoService;  //Service which will do all data retrieval/manipulation work
	  
	     
	    //-------------------Retrieve All tasks--------------------------------------------------------
	      
	    @RequestMapping(value = "/task/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<List<ToDoTask>> listAllTasks() {
	        List<ToDoTask> tasks = todoService.getAllTasks();
	        if(tasks.isEmpty()){
	            return new ResponseEntity<List<ToDoTask>>(HttpStatus.NO_CONTENT);
	            //You many decide to return HttpStatus.NOT_FOUND
	        }
	        return new ResponseEntity<List<ToDoTask>>(tasks, HttpStatus.OK);
	    }
	  
	  
	     
	    //-------------------Retrieve Single task--------------------------------------------------------
	      
	    @RequestMapping(value = "/task/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<ToDoTask> getTask(@PathVariable("id") long id) {
	        System.out.println("Fetching Task with id " + id);
	        ToDoTask task = todoService.findById(id);
	        if (task == null) {
	            System.out.println("Task with id " + id + " not found");
	            return new ResponseEntity<ToDoTask>(HttpStatus.NOT_FOUND);
	        }
	        return new ResponseEntity<ToDoTask>(task, HttpStatus.OK);
	    }
	  
	      
	      
	    //-------------------Create a task--------------------------------------------------------
	      
	    @RequestMapping(value = "/task/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<Void> createTask(@RequestBody ToDoTask task,    UriComponentsBuilder ucBuilder) {
	        System.out.println("Creating task " + task.getTaskDescription());
	  
	        if (todoService.isTaskExist(task)) {
	            System.out.println("A task with desc " + task.getTaskDescription() + " already exist");
	            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        }
	  
	        todoService.saveTask(task);
	  
	        HttpHeaders headers = new HttpHeaders();
	        headers.setLocation(ucBuilder.path("/task/{id}").buildAndExpand(task.getTaskID()).toUri());
	        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	    }
	  
	     
	      
	    //------------------- Update a task --------------------------------------------------------
	      
	    @RequestMapping(value = "/task/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<ToDoTask> updateTask(@PathVariable("id") long id, @RequestBody ToDoTask task) {
	        System.out.println("Updating task " + id);
	          
	        ToDoTask todoTask = todoService.findById(id);
	          
	        if (todoTask==null) {
	            System.out.println("task with id " + id + " not found");
	            return new ResponseEntity<ToDoTask>(HttpStatus.NOT_FOUND);
	        }
	  
	        todoTask.setTaskID(task.getTaskID());
	       todoTask.setTaskDescription(task.getTaskDescription());
	       todoTask.setPending(task.isPending());
	          
	       todoService.updateTask(todoTask);
	        return new ResponseEntity<ToDoTask>(todoTask, HttpStatus.OK);
	    }
	  
	     
	     
	    //------------------- Delete a TASK --------------------------------------------------------
	      
	    @RequestMapping(value = "/task/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<ToDoTask> deleteTask(@PathVariable("id") long id) {
	        System.out.println("Fetching & Deleting task with id " + id);
	  
	        ToDoTask task = todoService.findById(id);
	        if (task == null) {
	            System.out.println("Unable to delete. task with id " + id + " not found");
	            return new ResponseEntity<ToDoTask>(HttpStatus.NOT_FOUND);
	        }
	  
	        todoService.deleteTaskById(id);
	        return new ResponseEntity<ToDoTask>(HttpStatus.NO_CONTENT);
	    }
	  
	      
	     
	    //------------------- Delete All tasks --------------------------------------------------------
	      
	    @RequestMapping(value = "/task/", method = RequestMethod.DELETE)
	    public ResponseEntity<ToDoTask> deleteAllTasks() {
	        System.out.println("Deleting All tasks");
	  
	        todoService.deleteAllTasks();
	        return new ResponseEntity<ToDoTask>(HttpStatus.NO_CONTENT);
	    }
	 
	
	
	
}
